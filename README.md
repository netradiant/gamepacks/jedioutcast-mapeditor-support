# Jedi Knight Ⅱ: Jedi Outcast map editor support

This is a gamepack for [NetRadiant](https://netradiant.gitlab.io) providing support for Jedi Knight Ⅱ: Jedi Outcast.

This gamepack is based on the game pack from https://svn.icculus.org/gtkradiant-gamepacks/JK2Pack
